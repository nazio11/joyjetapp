const img1 = require('../src/assets/image1.jpg')
const img2 = require('../src/assets/image2.jpg')
const img3 = require('../src/assets/image3.jpg')
const img4 = require('../src/assets/image4.jpg')
const img5 = require('../src/assets/image5.jpg')
const img6 = require('../src/assets/image6.jpg')

const data = [
    {
        'id': 1,
        'image': img1,
        'titulo': 'Titulo 1',
        'descricao': 'Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        'autor': 'Autor 1',
        'favorite': false
    },
    {
        'id': 2,
        'image': img2,
        'titulo': 'Titulo 2',
        'descricao': 'Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        'autor': 'Autor 2',
        'favorite': false
    },
    {
        'id': 3,
        'image': img3,
        'titulo': 'Titulo 3',
        'descricao': 'Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        'autor': 'Autor 3',
        'favorite': false
    },
    {
        'id': 4,
        'image': img4,
        'titulo': 'Titulo 4',
        'descricao': 'Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        'autor': 'Autor 4',
        'favorite': false
    },
    {
        'id': 5,
        'image': img5,
        'titulo': 'Titulo 5',
        'descricao': 'Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        'autor': 'Autor 5',
        'favorite': false
    },
    {
        'id': 6,
        'image': img6,
        'titulo': 'Titulo 6',
        'descricao': 'Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        'autor': 'Autor 6',
        'favorite': false
    }
]


module.exports = data;
