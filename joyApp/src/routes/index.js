import React from 'react'

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import AppContainerDrawer from '../components/Drawer';
import InternalScreen from '../components/Internal';
//import FavoritesScreen from '../components/Favorite';

// PILHA DE ROTAS
const AppNavigator = createStackNavigator({
        HomeScreen: { screen: AppContainerDrawer },
        //FavoritesScreen: { screen: FavoritesScreen }, 
        InternalScreen: { screen: InternalScreen }, 
    },
    { headerMode: 'none' },
    { initialRouteName: 'HomeScreen' },
    
);

const AppContainer = createAppContainer(AppNavigator);

const Routes = () => { return <AppContainer /> }

export default Routes;