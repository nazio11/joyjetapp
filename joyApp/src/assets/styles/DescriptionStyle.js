import { StyleSheet } from 'react-native'

const DescriptionStyle = StyleSheet.create({
  view: {
    borderRadius: 20,
    position: "relative", 
    zIndex: 5,
    padding: 30, 
    marginBottom: 25,
    elevation: 6,
  },
  viewLabelDesc: {
    flexDirection: "row", 
    alignContent: "center", 
    alignItems: "center",
    justifyContent: "space-between",
    paddingRight: 6, 
    paddingBottom: 4
  },
  viewDesc: {
    flexDirection: "row", 
    alignContent: "center", 
    alignItems: "center",
    borderBottomColor: "#CCC", 
    borderBottomWidth: 1,
    marginBottom: 8, 
    paddingBottom: 8,
  },
  description: {
    fontFamily: "Poppins-Regular", 
    fontSize: 20, 
    alignSelf: 'flex-start' 
  },
  author: {
    fontFamily: "Poppins-Regular", 
    fontSize: 15 
  }
})

module.exports = DescriptionStyle