import { StyleSheet } from 'react-native'

const HeaderStyle = StyleSheet.create({
    container: { 
        height: 70, 
        backgroundColor:'#FFF' 
    },
    title: {
        fontSize: 30,
        fontWeight: 'normal',
        color: '#4A90E2',
        fontFamily: 'Montserrat',
        textAlign: 'center',
        width: "100%"
    }
})

module.exports = HeaderStyle