import { StyleSheet } from 'react-native'

const HomeStyle = StyleSheet.create({
  title: {
    fontSize: 30,
    fontWeight: 'normal',
    color: '#4A90E2',
    fontFamily: 'Montserrat',
    textAlign: 'center',
    width: "100%"
}
})

module.exports = HomeStyle