import { StyleSheet } from 'react-native'

const ItemSelectedStyle = StyleSheet.create({
  image: { 
    height: 300, 
    margin: 10
  },
  title: {
    color: '#000', 
    fontSize: 25, 
    margin: 10, 
  },
  icon: {
    color: '#CCC', 
    fontSize: 35, 
    margin: 10, 
    position: "absolute", 
    top: '5%', 
    right: 8,
  },
  favorited: {
    color: 'yellow', 
    fontSize: 35, 
    margin: 10, 
    position: "absolute", 
    top: '5%', 
    right: 8,
  }
})

module.exports = ItemSelectedStyle