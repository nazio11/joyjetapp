import { StyleSheet } from 'react-native'

const ItemStyle = StyleSheet.create({
  image: { 
    height: 300, 
    margin: 10
  },
  title: {
    color: '#FFF', 
    fontSize: 25 , 
    margin: 10, 
    position: "absolute", 
    bottom: '15%', 
    left: 8
  },
  description: {
    color: '#CCC', 
    fontSize: 20 , 
    margin: 10, 
    position: "absolute", 
    bottom: '10%', 
    left: 10
  },
  author: {
    color: '#CCC', 
    fontSize: 15 , 
    margin: 10, 
    position: "absolute", 
    bottom: '5%', 
    left: 10
  }
})

module.exports = ItemStyle