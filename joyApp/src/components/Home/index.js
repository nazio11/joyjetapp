import React from 'react'
import { View, StatusBar, ScrollView } from 'react-native'
import HomeStyle from '../../assets/styles/HomeStyle'

import ListScreen from '../../container'
import HeaderScreen from '../Header';

const HomeScreen = (props) => {
    return (
        <View style={{flex:1}}>
            <StatusBar backgroundColor="rgba(49,49,49,0)" translucent={true} barStyle="dark-content" />
            <HeaderScreen
                leftComponent={{   
                    icon: 'menu',                     
                    onPress: () => { props.navigation.openDrawer() },                       
                }}
                centerComponent={{
                    text: 'Joyjet',
                    style: HomeStyle.title
                }}
            />
            <ScrollView showsVerticalScrollIndicator={false}>
                <ListScreen />
            </ScrollView>
        </View>
    )
}

export default HomeScreen

//https://www.figma.com/file/8364HvmSmcjrGpigxMJUmO/joyjet-apptest-ios?node-id=0%3A1,
//https://bitbucket.org/lixao/tech-interview/src/master/android/


