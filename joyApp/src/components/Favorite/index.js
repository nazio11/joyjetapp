import React, { useState, useEffect }  from 'react'
import HeaderScreen from '../Header';
import HeaderStyle from '../../assets/styles/HeaderStyle'

import AsyncStorage from '@react-native-community/async-storage';
import { View, Image, Text, ScrollView, Alert, Dimensions } from 'react-native';

import ItemStyle from '../../assets/styles/ItemStyle' 

const FavoritesScreen = (props) => {

    const [favorites, setFavorites] = useState([])

    const getFavorite = async () => {
    
        try {
            const value = await AsyncStorage.getItem('farorites')
            if(value !== null) {

                var object = JSON.parse(value)
                
                setFavorites(object)

            }
        } catch(e) {
            console.log("Erro ao recuperar os items favoritos!")
            Alert.alert("Erro ao recuperar seus items favorito!")
        }
    }

    useEffect(() => { 
        getFavorite()
    })

    return (

        <View style={{flex: 1}}>
            <HeaderScreen
                leftComponent={{   
                    icon: 'menu',                     
                    onPress: () => { props.navigation.openDrawer() },                       
                }}
                centerComponent={{
                    text: 'Favorites',
                    style: HeaderStyle.title
                }}  
            />

            <ScrollView showsVerticalScrollIndicator={false}>
                {
                    favorites.length > 0 ?
                        (
                            favorites.map(item => {
                                return (
                                    <View key={item.id}>
                                        <Image source={item.image} style={ItemStyle.image} />
                                        <Text style={ItemStyle.title}> {item.titulo} </Text>
                                        <Text style={ItemStyle.description}> {item.descricao.slice(0,35)}...</Text>
                                        <Text style={ItemStyle.author}> {item.autor}</Text>   
                                    </View>
                                )
                            })
                        )
                    :
                    <View style={{height: Dimensions.get('window').height, justifyContent: 'center'}}>
                        <Text style={{fontSize: 25, color: "#ccc", fontWeight: 'bold', textAlign: 'center'}}> Nenhum item favorito! </Text>
                    </View>
                }
            </ScrollView>           
        </View>
    )
}
export default FavoritesScreen