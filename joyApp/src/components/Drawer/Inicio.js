import React from 'react'
import { Icon } from 'native-base';
import { Text, View } from 'react-native';

const INICIO = () => { 
    return (
        <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <Icon name="home" size={20} style={{ color: "#CCC", width: 30 }} />
            <Text style={{alignSelf: 'center', fontSize: 20, color: '#FFF'}}> Home </Text>
        </View>
    )
     
}

export default INICIO