import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';

// ROTAS DO MENU LATERAL
import HomeScreen from '../Home';
import FavoritesScreen from '../Favorite';

import INICIO from './Inicio';
import FAVORITES from './Favorite';
import MENU from './Menu';

// CONFIGURACAO MENU LATERAL ICONE-TEXT
const MENU_LATERAL = createDrawerNavigator(
    {              
        HomeScreen: { 
            screen: HomeScreen,
            navigationOptions: {
                drawerIcon: INICIO,
                drawerLabel: 'Início',
            },
        }, 
        FavoritesScreen: { 
            screen: FavoritesScreen,
            navigationOptions: {
                drawerIcon: FAVORITES,
                drawerLabel: 'Favorites',
            },
        }
    },
    {
        drawerBackgroundColor: '#434343',
        contentComponent: MENU,
        contentOptions: {
            labelStyle: { fontSize: 20, color: '#FFFFFF' },
        },
        drawerPosition: 'right'        
    }
);

const AppContainerDrawer = createAppContainer(MENU_LATERAL);

export default AppContainerDrawer;