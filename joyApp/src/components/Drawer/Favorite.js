import React from 'react'
import { Icon } from 'native-base';
import { Text, View } from 'react-native';

const FAVORITES = () => { 
    return (
        <View style={{ flexDirection: 'row', marginTop: 30 }}>
            <Icon name="star" size={20} style={{ color: "#CCC", width: 30 }} />
            <Text style={{alignSelf: 'center', fontSize: 20, color: '#FFF'}}> Favorites </Text>
        </View>
    ) 
}


export default FAVORITES