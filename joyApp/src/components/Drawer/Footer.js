import React from 'react'
import { View, Text } from 'react-native';

const FOOTER = () => {
    return (
        <View style={{height: 100, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{fontSize: 25, fontWeight: 'bold', color: '#ccc'}}> JoyJet </Text>
        </View>
    )
}

export default FOOTER