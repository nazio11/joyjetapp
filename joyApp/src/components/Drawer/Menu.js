import React from 'react'
import { View, TouchableOpacity } from 'react-native';

import INICIO from './Inicio';
import FAVORITES from './Favorite';
import FOOTER from './Footer';

const MENU = (props) => {
    return (
        <View style={{flex: 1, justifyContent: 'space-between', margin: 10 }}>
            <View>
                <TouchableOpacity onPress={() => props.navigation.navigate("HomeScreen")}>
                    <INICIO />
                </TouchableOpacity> 

                <TouchableOpacity onPress={() => props.navigation.navigate("FavoritesScreen")}>
                    <FAVORITES />
                </TouchableOpacity>
            </View>
            <FOOTER />
        </View>
    )
}

export default MENU;