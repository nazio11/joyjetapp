import React from 'react'
import { View, StatusBar, ScrollView } from 'react-native'
import { withNavigation } from 'react-navigation';
import HomeStyle from '../../assets/styles/HomeStyle'
import ItemSelected from '../../container/ItemSelected';
import HeaderScreen from '../Header';

const InternalScreen = (props) => {
    
    let data = props.navigation.state.params

    return (
        <View style={{flex: 1}}>
            <StatusBar backgroundColor="rgba(49,49,49,0)" translucent={true} barStyle="dark-content" />
            <HeaderScreen
                leftComponent={{   
                    icon: 'arrow-back',                     
                    onPress: () => { props.navigation.goBack() },                       
                }}
                centerComponent={{
                    text: 'Description',
                    style: HomeStyle.title
                }}
            />

            <ScrollView showsVerticalScrollIndicator={false}>
                <ItemSelected 
                    id={data.data.id} 
                    image={data.data.image} 
                    titulo={data.data.titulo} 
                    descricao={data.data.descricao} 
                    autor={data.data.autor} 
                    navigate={props.navigation}
                    favorite={data.data.favorite}
                />
            </ScrollView>

        </View>
    )
}
export default withNavigation(InternalScreen);