import React from "react";
import { Header } from "react-native-elements";
import HeaderStyle from '../../assets/styles/HeaderStyle'

const HeaderScreen = (props) => {
    return (
        <Header
            placement="right"
            containerStyle={HeaderStyle.container}                                                                          
            centerComponent={props.centerComponent}
            rightComponent={{ 
                icon: props.leftComponent.icon, 
                onPress: props.leftComponent.onPress,
                size: 30, 
                color: '#434343' 
            }}                        
        />
    )
}

export default HeaderScreen