import AsyncStorage from '@react-native-community/async-storage';
import { Alert } from 'react-native';

const listObject = []

const saveFavorite = async(data) => {

    var data = {
        id: data.id,
        image: data.image,
        titulo: data.titulo,
        descricao: data.descricao,
        autor: data.autor,
    }

    listObject.push(data)

    var object = JSON.stringify(listObject)

    try {
        console.log("Salvo com sucesso!")
        await AsyncStorage.setItem('farorites', object)

    } catch (e) {
        console.log("Erro ao salvar seus dados.")
        Alert.alert("Erro ao Salvar seu item favorito!")
    }
}

export default saveFavorite