import React,{ useState } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { withNavigation } from 'react-navigation';
import Description from './Description';

import ItemSelectedStyle from '../assets/styles/ItemSelectedStyle'

import { useDispatch } from 'react-redux'
import { Icon } from 'native-base';

const ItemSelected = (props) => {

    const [favorite, setFavorite] = useState(false)
    
    const clicked = () => {
        
        setFavorite(!favorite) 

        const newObject = Object.assign({}, props, { favorite: !favorite })

        FAVORITE(newObject)
    }
    
    const dispatch = useDispatch()

    const FAVORITE = (props) => {
        dispatch({ type: 'SAVEFAVORITE',  params: props })
    }

    return (
        <View>
            <TouchableOpacity onPress={() => clicked()}> 
                <Image source={props.image} style={ItemSelectedStyle.image} />
                <Icon name='star' style={favorite ? ItemSelectedStyle.favorited : ItemSelectedStyle.icon} />
            </TouchableOpacity>
            <Text style={ItemSelectedStyle.title}>{props.titulo}</Text>

            <Description 
                descricao={props.descricao} 
                autor={props.autor} 
                navigate={props.navigation}
            />
        </View>
    )
}

export default withNavigation(ItemSelected)