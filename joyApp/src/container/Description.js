import React from 'react';
import { View, Text } from 'react-native';
import DescriptionStyle from '../assets/styles/DescriptionStyle'

function Description(props) {

    return (
        <View style={DescriptionStyle.view}>
            <View style={DescriptionStyle.viewLabelDesc}>
                <Text style={DescriptionStyle.description}>Descrição</Text>
            </View>

            <View style={DescriptionStyle.viewDesc}>
                <Text style={DescriptionStyle.description}>{ props.descricao }</Text>
            </View>

            <Text style={DescriptionStyle.author}>{ props.autor }</Text>
        </View>
    )
}

export default Description;