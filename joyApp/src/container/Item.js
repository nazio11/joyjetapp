import React from 'react'
import { Text, Image, TouchableOpacity } from 'react-native'
import { withNavigation } from 'react-navigation';
import ItemStyle from '../assets/styles/ItemStyle'

import { useDispatch } from 'react-redux'

const Item = (props) => {

    const dispatch = useDispatch()

    const SELECTED = (props) => {
        dispatch({ type: 'SELECTED',  params: props })
    }

    return (
        <TouchableOpacity onPress={() => SELECTED(props)}>
            <Image source={props.image} style={ItemStyle.image} />
            <Text style={ItemStyle.title}> {props.titulo} </Text>
            <Text style={ItemStyle.description}> {props.descricao.slice(0,35)}...</Text>
            <Text style={ItemStyle.author}> {props.autor}</Text>
        </TouchableOpacity> 
    )
}

export default withNavigation(Item)