import React, { useState, useEffect }  from 'react'
import { View, StatusBar } from 'react-native'
import { withNavigation } from 'react-navigation';
import { useSelector } from 'react-redux'
import Item from './Item';

const ListScreen = (props) => {
    const [lista, setLista] = useState([])

    const array = useSelector(state =>  state.data )
    
    useEffect(() => { setLista(array) }, [lista])

    return (
        <View style={{flex: 1}}>
            <StatusBar backgroundColor="rgba(49,49,49,0)" translucent={true} barStyle="dark-content" />
            
            {
                lista.map((item) => {

                    return (
                        <View key={item.id}>
                            <Item 
                                id={item.id} 
                                image={item.image} 
                                titulo={item.titulo} 
                                descricao={item.descricao} 
                                autor={item.autor} 
                                navigate={props.navigation}
                                favorite={item.favorite}
                            />
                        </View>
                    )

                })
            }

        </View>
    )
}

export default withNavigation(ListScreen);

