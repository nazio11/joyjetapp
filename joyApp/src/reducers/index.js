import db from '../../db/db'
import saveFavorite from '../functions/saveFavorite';

const INITIAL_STATE = { data: db }

const reducer = (state = INITIAL_STATE, action ) => {

    const data = action.params

    switch(action.type) {
        case "SELECTED":            
            return data.navigate.navigate('InternalScreen', { data: data })
        case "SAVEFAVORITE":
            saveFavorite(data)
        default: 
            return state;
    }
}

export default reducer;